'''@file        lab3.py
@brief          A program that plays simone says with the user.
@details        Link to Source Code: https://bitbucket.org/ebriefer/me305_lab/src/master/Lab%203/lab3.py
                Demonstration Video: https://youtu.be/LWRrAM0yLX0
                This program randomly generates a sting of LED flashes of varying length.
                Then, it compares the durration of button presses by the user to the pattern, and
                if they are sufficiently close, congratulates the user and generates another random
                sequence for them to replicate.  The pattern length and difficulty gradually 
                increases as the user progresses.
                State machine diagram:
@image html     lab3.jpg
@author         Eliot Briefer
@date           2/18/2020
'''
import random, utime, pyb

class simonSays:
    '''
    @brief A class that plays simon says with the user using an LED and a button.
    @details The simonSays class randomly generates a sting of LED flashes of varying length.
    Then, it compares the durration of button presses by the user to the pattern, and
    if they are sufficiently close, congratulates the user and generates another random
    sequence for them to replicate.
    '''
    
    ## @brief dictionary of messages to display at each state change
    stateMessages = {
        0 : "Welcome to Simon Says! The Nucleo will flash a pattern on using the LED."
            + "Then, repeate the pattern by pressing the blue button. If you get it"
            + "right you'll move to the next level, if you get it wrong you loose!",
        1 : "Press the blue button to play!",
        2 : "Playing Sequence. Pay close attention!",
        3 : "Repeat the pattern by pressing the blue button",
        4 : "Continue Pattern",
        5 : "Press the blue button to proceed to the next level."
        }
    
    ## @brief List of level numbers to increase the level length at.
    incLengthAt = [4, 6, 10, 15, 20]
    ## @brief List of level numbers to increase the speed (tempo) at.
    incTempoAt = [2, 3, 5, 9, 11, 13, 14, 15, 16, 17, 18, 19, 20]
    ## @brief List of level numbers to increase the max flash durration at.
    incmaxFlashLengthAt = [2, 5, 12, 20]
    def __init__(self, pwm, startState = 0):
        '''
        @brief A constructor that initializes all data stored in a simonSays object.
        @param pwm The pwm object for the simonSays object to output to, usually used to control an LED brightness.
        @param startState the startup state for the simonSays object to initialize in.
        '''
        self.currentLevel = []
        ## @brief The user's highest score since startup.
        self.highScore = 0
        ## @brief The user's current score.
        self.currentScore = 0
        ## @brief The current level number the user is on
        self.levelNumber = 1
        ## @brief The speed of the pattern, in seconds/beat
        self.tempo = 1
        ## @brief The length of the levels, in number of flashes
        self.levelLength = 3
        ## @brief The max durration, in beats, of a flash or rest
        self.maxFlashLength = 1
        ## @brief The current state of the button (True = pressed)
        self.buttonState = False
        ## @brief The current state of the finite state machine
        self.state = 0
        self.lastTransition = utime.ticks_ms()
        ## @brief Wether or not the button state has changed (True = pressed or released).
        self.buttonPress = False
        self.buttonRelease = False
        self.buttonState = False
    
        self.pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
        self.ButtonChangeInt = pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_FALLING | pyb.ExtInt.IRQ_RISING, pull=pyb.Pin.PULL_NONE, callback=self.onButtonChangeFCN)
        self.transitionStates(True, startState)
        self.pwm = pwm
       
    def runStateMachine(self):
        '''
        @brief A function that runs through the state machine logic of the simonSays object once.
        '''
        if self.state == 0:
            #Welcome screen
            self.transitionStates(True, 1)
            
        if self.state == 1:
            #you lose, display high score, press to play again
            self.currentLevel = self.generateLevel(self.levelLength, self.maxFlashLength)
            self.transitionStates(self.buttonRelease, 2)
            
        if self.state == 2:
            #display level
            self.pwm.pulse_width_percent(self.displayLevel(self.currentLevel, utime.ticks_ms(), tempo = self.tempo))
            
            
        if self.state == 3:
            #wait for first button press
            self.transitionStates(self.buttonPress, 4)
            
        if self.state == 4:
            #listen for user repeate
            self.listenForLevel(self.currentLevel, utime.ticks_ms(), tempo = self.tempo)
            
        if self.state == 5:
            #Level passed, press button to proceed to next level
            self.currentLevel = self.generateLevel(self.levelLength, self.maxFlashLength)
            self.transitionStates(self.buttonRelease, 2)
        
    def generateLevel(self, length, maxDurration):
        '''
        @brief A function that creates the pattern for the user to replicate
        @param
        @return A list of time durrations for button flashed and gaps.
        '''
        return [random.randrange(1, maxDurration + 1) for _ in range(0, length * 2 - 1)]
    
    def displayLevel(self, level, tick, tempo = 1):
        '''
        @brief A function that flashes the LED in a randomly generated pattern
        @param level A 1D list of durrations to turn the LED on and off in a pattern
        @param tick The current system time in ms
        @param tempo the speed at which to display the pattern (1 = 1 time unit is 1 sec, 2 = 1 time unit is 2 sec etc.)
        @return The duty cycle of the LED to turn it on or off based on the generated pattern of flashed
        '''
        diff = utime.ticks_diff(tick, self.lastTransition)/1000
        currentDurration = 0
        i = 0
        while (currentDurration < diff) and (i < len(level)):
            #Figure out which index the pattern is on based on the elapsed time
            currentDurration += level[i] * tempo
            i += 1
        if i > 0:i -= 1
        #print("level length: " + str(len(level)) + " current index: " + str(i))
        if currentDurration < diff:
            self.transitionStates(True, 3)
            print("Pattern: " + str(level))
            return 0
        elif i % 2 == 0:
            return 100
        else: 
            return 0
    
    def listenForLevel(self, level, tick, tolerance = 0.25, tempo = 1):
        '''
        @brief A function that compares the timing of the user buttom presses to the previously displayed pattern.
        @param level A 1D list of durrations to compare to the user button presses.
        @param tick The current system time in ms.
        @param tolerance The maximum allowable difference between the time the user pressed the button and the desired time.
        @param tempo The time durration of one beat.
        '''
        if self.pinC13.value() == 0: 
            self.pwm.pulse_width_percent(100)
        else:
            self.pwm.pulse_width_percent(0)
        
        if len(level) == 0:
            self.transitionStates(True, 5, customMessage = "Level " + str(self.levelNumber) + " Passed! Score: " + str(self.currentScore))
            self.currentScore += 10
            if self.levelNumber in self.incLengthAt: self.levelLength += 1
            if self.levelNumber in self.incmaxFlashLengthAt: self.maxFlashLength += 1
            if self.levelNumber in self.incTempoAt: self.tempo *= 0.9
            self.levelNumber += 1
            
        if self.buttonPress or self.buttonRelease:
            #print(level)
            diff = utime.ticks_diff(tick, self.lastTransition)/1000
            if abs(diff - level[0] * tempo) <= tolerance:
                level.pop(0)
                self.transitionStates(True, 4, customMessage = "Pattern: " + str(level), checkBounce = True)
                self.currentScore += 1
            else:
                print('Time you pressed: ' + str(diff) + ' sec. Time desired: ' + str(level[0] * tempo) + " sec.")
                if self.currentScore > self.highScore:
                    self.highScore = self.currentScore
                self.transitionStates(True, 1, customMessage = "You Lose! Score: " + str(self.currentScore) + " Highscore: " + str(self.highScore), checkBounce = True)
                self.currentScore = 0
                self.levelNumber = 1
                self.levelLength = 3
                self.maxFlashLength = 1
                self.tempo = 1
                
                
        
    def onButtonChangeFCN(self, IRQ_src):
        '''
        @brief A function that sets buttonChange to True when the blue button state changes and negates the current button state.
        @param IRQ_src The pin number that triggered the inturrupt.
        '''
        if self.pinC13.value() == 0:
            self.buttonPress = True
            self.buttonRelease = False
            self.buttonState = True
            
        if self.pinC13.value() == 1:
            self.buttonPress = False
            self.buttonRelease = True
            self.buttonState = False
        
    def transitionStates(self, trans, st, customMessage = "", checkBounce = False):
        '''
        @brief A function that preforms all repetative tasks durring a state change.
        @param trans A boolean representing wether to transition or not (True = transition now).
        @param st The state to transition to.
        @param customMessage A string to print in addition to the standard state message.
        @param checkBounce A boolean to check if the button pressed time was unreasonably short to check elminate button bounce.
        This is used to prevent the state from changing until the button is both pressed and released.
        '''
        
        if trans and (not checkBounce or abs(self.lastTransition - utime.ticks_ms())/1000 > 0.1):
            self.state = st
            print(customMessage)
            print(self.stateMessages[st])
            self.lastTransition = utime.ticks_ms()
            self.buttonPress = False
            self.buttonRelease = False
            #print('last transition occured at: ' + str(self.lastTransition / 1000))
            
if __name__=="__main__":
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    game = simonSays(t2ch1)
    while True:
        game.runStateMachine()
        #print(game.pinC13.value())
            
        
        

