'''@file        lab2.py
@brief          A program that implements a state machine for changing the flashing pattern of an LED
@details        Link to Source Code: https://bitbucket.org/ebriefer/me305_lab/src/master/Lab%202/lab2.py
                Demonstration video: https://youtu.be/98HGiMh8p0U
                This program cycles through three different flashing patterns 
                for and LED on the nucleo board. Specifically, the three patterns 
                are a square wave, a sine wave, and sawtooth wave. The LED pattern
                changes every time the user presses the blue button on the nucleo.
                State machine diagram:
@image html     test.png
@author         Eliot Briefer
@date           2/4/2020
'''
import pyb, utime
from math import sin

def onButtonPressFCN(IRQ_src):
    '''
    @brief A function that sets buttonPress to True when the blue button is pressed
    '''
    global buttonPress
    buttonPress = True
    
def squareWave(tick):
    '''
    @brief A function that generates a square wave that oscillates between 0 and 100.
    @param tick The current clock tick in milliseconds.
    @return The value of the square wave at the given tick.
    '''
    diff = utime.ticks_diff(tick, lastTransition)/1000
    #print(str(diff))
    return round(diff % 1) * 100

def sineWave(tick):
    '''
    @brief A function that generates a sine wave that oscillates between 0 and 100.
    @param tick The current clock tick in milliseconds.
    @return The value of the sine wave at the given tick.
    '''
    diff = utime.ticks_diff(tick, lastTransition)/1000
    return 50 + sin(diff) * 50

def sawtoothWave(tick):
    '''
    @brief A function that generates a sawtooth wave that oscillates between 0 and 100.
    @param tick The current clock tick in milliseconds.
    @return The value of the sawtooth wave at the given tick.
    '''
    diff = utime.ticks_diff(tick, lastTransition)/1000
    return (diff % 1) * 100

def transitionStates(trans, st):
    '''
    @brief A function that preforms all repetative tasks durring a state change.
    @param trans A boolean representing wether to transition or not (True = transition now).
    @param st The state to transition to.
    '''
    if trans:
        global state
        global buttonPress
        global lastTransition
        global stateMessage
        state = st
        print(stateMessages[state])
        buttonPress = False
        lastTransition = utime.ticks_ms()


if __name__ == '__main__':
    ## @brief   The current state of the elevator finite state machine
    state = 0
    lastTransition = utime.ticks_ms()
    ## @brief Wether or not the button is pressed (True = pressed).
    buttonPress = False
    
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    stateMessages = {
        0 : "Square wave pattern selected",
        1 : "Sine wave pattern selected",
        2 : "Sawtooth wave pattern selected"
        }
    
    print('Welcome! Press the blue button to change the LED pattern')
    transitionStates(True, 0)
    
    while True:
        dutyCycle = 0
    
        if state == 0:
            transitionStates(buttonPress, 1)
            dutyCycle = squareWave(utime.ticks_ms())
            #print(str(dutyCycle))
            
        elif state == 1:
            transitionStates(buttonPress, 2)
            dutyCycle = sineWave(utime.ticks_ms())
            #print(str(dutyCycle))
            
        elif state == 2:
            transitionStates(buttonPress, 0)
            dutyCycle = sawtoothWave(utime.ticks_ms())
            #print(str(dutyCycle))
            
        else:
            print("invalid state:" + str(state))
            transitionStates(True, 0)
        
        #print(str(dutyCycle))
        t2ch1.pulse_width_percent(dutyCycle)


    