'''
@file controller.py
@brief a controller task for controlling a motor using PID.
@details The controller task is a state machine that uses closed loop
feedback to move a motor along a reference path and velocity. The controller
task also has several methods, selfTune, step, and ramp, for automatically tunning the PID gains
and are not implemented as part of the state machine. These methods are only
for debugging and tuning and should not be run with this, or any other state machine,
as they will pause the program until complete.
Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Final%20Project/controller.py
@ingroup week3
@author Eliot Briefer
'''

import utime, pyb, shares
from encoderDriver import Encoder
from motorDriver import Motor
from pid import ClosedLoop

class Controller:
    '''
    @brief A state machine to control a motor using PID.
    @details The controller task is a state machine that uses closed loop
    feedback from an encoderDriver.encoder object to move a motor, controlled using a motorDriver.motor object
    along a reference path and velocity. The controller
    task also has several methods, controller.Controller.selfTune, step, and ramp, for automatically tunning the PID gains
    and are not implemented as part of the state machine. These methods are only
    for debugging and tuning and should not be run with this, or any other state machine,
    as they will pause the program until complete.
    Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Final%20Project/controller.py
    @ingroup week3
    '''
    

    ## @brief A Dictionary of messages to display at each state change.
    stateMessages = {0 : 'State 0: startup',
                     1 : 'State 1: Enabled',
                     2 : 'State 2: Wating',
                     3 : 'State 3: Reseting',
                     4 : 'State 4: Error'}
    
    def __init__(self, motor, encoder, closedLoop, state = 0):
        '''
        @brief Method to initialise a Controller object.
        @param motor A Motor object to control.
        @param encoder An Encoder object to get feedback from the motor.
        @param closedLoop A ClosedLoop object to calculate the needed motor PWM level.
        @param state The initial state to start the state machine in.
        '''
        pyb.repl_uart(None)
        ##@brief The Motor object to control.
        self.motor = motor
        self.motor.disable()
        ##@brief The Encoder object to get feedback from the motor.
        self.encoder = encoder
        ##@brief The ClosedLoop object to calculate closed loop control PWM level.
        self.closedLoop = closedLoop
        ##@brief The current state of the state machine.
        self.state = state
        ##@brief The position of self.motor
        self.position = 0 #in rotations
        ##@brief The current target position for the motor.
        self.targetPosition = 0
        ##@brief The current velocity of self.motor.
        self.velocity = 0 #in rotations per second.
        ##@brief The current target velocity of the motor.
        self.targetVelocity = 0
        ##@brief The last time (in ms from startup) that self.update() was called.
        self.prevTime = utime.ticks_ms()
        ##@brief The last time (in ms from startup) that the state machine transitioned states.
        self.lastTransition = utime.ticks_ms()
        self.transitionStates(0)
        ##@brief The .csv reference file of format time(s), velocity (rpm), position (deg) to control the motor to.
        self.referenceFile = None #Reference file should be 3 column CSV of [[time(s), pos(rotations), velocity(rotations per second)], ...]
        ##@brief The current point reference point, format [time, velocity, position].
        self.referencePoint = [0,0,0] #Time, velocity, position
        
    def runStateMachine(self):
        '''
        @brief A method that runs through the logic of the controller state machine once.
        '''
        if self.state == 0:
            try:
               self.referenceFile = open('reference.csv')
               self.transitionStates(2)
            except:
                print('Error opening reference.csv')
                self.transitionStates(4)
            
            
        if self.state == 1:
            self.update()
            print('pos: ' + str(self.position) + ' rotations, velocity: ' + str(self.velocity) + 'rps')
            #CSV input
            if utime.ticks_diff(utime.ticks_ms(), self.lastTransition)/1000 > self.referencePoint[0]:
                self.getNextReferencePosition(self.referenceFile)
                print(self.referencePoint)
                shares.logMotorState([utime.ticks_diff(utime.ticks_ms(), self.lastTransition)/1000, self.velocity, self.position])
            self.targetVelocity = self.referencePoint[1]
            self.targetPosition = self.referencePoint[2]
            
            self.pointControl(self.targetPosition, self.targetVelocity)
            #self.pointControl(0, 5)
            
            if utime.ticks_diff(utime.ticks_ms(), self.lastTransition) / 1000 > 15:
                self.transitionStates(2)
                shares.collectData = False
                print('J: '+ str(self.closedLoop.get_J()))
            
        if self.state == 2:
            self.motor.set_duty(0)
            self.motor.disable()
            if shares.collectData:
                self.transitionStates(3)
            
        if self.state == 3:
            self.transitionStates(1)
            self.encoder.set_position(0)
            self.motor.enable()
            self.referenceFile.seek(0,0)
            
        if self.state == 4:
            print('Controller error state')
            
    
    def transitionStates(self, state, transition = True):
        '''
        @brief A method that changes the state machine state, self.state and prints the state transition message.
        @param state The state to transition to.
        @param transition A boolean for wether or not to transition, if False no transition will occur.
        '''
        if transition:
            self.state = state
            self.lastTransition = utime.ticks_ms()
            print(self.stateMessages[state])
            
    def pointControl(self, pos, vel):
        '''
        @brief A method to calculate the motor PWM to get to a given position and velocity.
        @param pos The target position to move the motor to, in rotations.
        @param vel The target velocity to move the motor at, in rotations per second.
        @return The motor PWM level, from -100 to 100 (or capped at self.closedLoop saturation level)
        '''
        self.update()
            
        velocityError = vel - self.velocity
        positionError = pos - self.position
        response = self.closedLoop.update(positionError, velocityError)
            
        self.motor.set_duty(response)
        return response
    
    def update(self):
        '''
        @brief A method to check the encoder position and velocity
        '''
        #update position and velocity
        self.encoder.update()
        deltaT = utime.ticks_diff(utime.ticks_ms(), self.prevTime) / 1000
        self.velocity = (self.encoder.get_position() - self.position) / deltaT
        self.position = self.encoder.get_position()
        self.prevTime = utime.ticks_ms()
        #print('pos: ' + str(self.position) + ' rotations, velocity: ' + str(self.velocity) + 'rps')
            
    def getNextReferencePosition(self, file):
        '''
        @brief A method that updates self.referencePoint to the next line in the reference csv.
        @param file The File object to read from, should be a .csv of the same format as self.referenceFile.
        '''
        words = []
        endOfFile = False
        try:
            line = file.readline()
            if line is not None:
                words = [float(word.strip()) for word in line.split(',')]
                words[1] = words[1] / 60 #convert from rpm to rps
                words[2] = words[2] /360 #convert from degrees to rotations
            else:
                endOfFile = True
        except:
            print('Error reading line in reference file.')
            
        if not endOfFile:
            if len(words) == 3:
                self.referencePoint = words
            else:
                print('Invalid line format: expected 3 values got: ' + str(len(words)))
        else:
            self.transitionStates(2)
            shares.collectData = False
            print('J: '+ str(self.closedLoop.get_J()))
            
    #NOT STATE MACHINE, FOR TUNING ONLY
    def step(self, magnitude, timeOut, delayms = 20):
        '''
        @brief Method to subject the motor to a step function.
        @details This method is not implemented for use in a state machine and
        should not be run in conjucntion with any state machines. This method is
        only for testing and tuning.
        @param magnitude The magnitude of the step function, in rotations.
        @param timeOut The durration to run the step function for, in seconds.
        @param delayms The period to update the motor control at, in ms.
        @return The J value of the controler's step response'
        '''
        self.encoder.set_position(0)
        self.closedLoop.reset_J()
        self.motor.enable()
        startTime = utime.ticks_ms()
        prevTime = utime.ticks_ms()
        while utime.ticks_diff(utime.ticks_ms(), startTime)/1000 < timeOut:
            if utime.ticks_diff(utime.ticks_ms(), prevTime) > delayms:
                self.pointControl(magnitude, 0)
                prevTime = utime.ticks_ms()
        self.motor.disable()
        return self.closedLoop.get_J()
            
    def selfTune(self, pos, vel, timeOut = 5, delayms = 20, pIn = 1, iIn = 0.01):
        '''
        @brief Method to self tune the PI gains.
        @details This method is not implemented for use in a state machine and
        should not be run in conjucntion with any state machines. This method is
        only for testing and tuning.
        @param pos The magnitude of the step function used for tuning Kp, in rotations.
        @param vel The magnitude of the ramp function used for tuning Kd, in rotaions per second.
        @param timeOut The duration of each test, in seconds
        @param delayms The period to update the motor controler at, in ms
        @param pIn The initial Kp to guess. should be the minimum realistic Kp, not 0.
        @param iIn The initial Ki to guess. should be the minimum realistic Ki, not 0.
        '''
        p = pIn
        i = 0
        d = 0
        
        self.closedLoop.set_Kp(p)
        self.closedLoop.set_Ki(i)
        self.closedLoop.set_Kd(d)
        
        bestJ = self.step(pos, timeOut)
        p *= 2
        self.closedLoop.set_Kp(p)
        currentJ = self.step(pos, timeOut)
        print('J: ' + str(currentJ) + ', K_p: ' + str(p))
        n = 0
        
        #TUNNING P
        while currentJ < bestJ and n < 10:
            n += 1
            bestJ = currentJ
            p *= 2
            self.closedLoop.set_Kp(p)
            currentJ = self.step(pos, timeOut)
            print('J: ' + str(currentJ) + ', K_p: ' + str(p))
        bestJ = currentJ
        p *= 0.5
        self.closedLoop.set_Kp(p)
        print('J: ' + str(currentJ) + ', K_p: ' + str(p))
        currentJ = self.step(pos, timeOut)
        #while currentJ < bestJ and n < 10:
            #n += 1
            #bestJ = currentJ
            #p *= 0.85
            #self.closedLoop.set_Kp(p)
            #currentJ = self.step(pos, timeOut)
            #print('J: ' + str(currentJ) + ', K_p: ' + str(p))
        #print('J: ' + str(currentJ) + ', K_p: ' + str(p))
        
        #TUNNING I
        i = iIn
        bestJ = self.step(pos, timeOut)
        i *= 2
        self.closedLoop.set_Ki(i)
        currentJ = self.step(pos, timeOut)
        print('J: ' + str(currentJ) + ', K_i: ' + str(i))
        n = 0
        while currentJ < bestJ and n < 10:
            n += 1
            bestJ = currentJ
            i *= 2
            self.closedLoop.set_Ki(i)
            currentJ = self.step(pos, timeOut)
            print('J: ' + str(currentJ) + ', K_i: ' + str(i))
        bestJ = currentJ
        i *= 0.85
        self.closedLoop.set_Ki(i)
        print('J: ' + str(currentJ) + ', K_p: ' + str(p))
        currentJ = self.step(pos, timeOut)
        while currentJ < bestJ and n < 10:
            n += 1
            bestJ = currentJ
            i *= 0.85
            self.closedLoop.set_Ki(i)
            currentJ = self.step(pos, timeOut)
            print('J: ' + str(currentJ) + ', K_i: ' + str(i))
        print('J: ' + str(currentJ) + ', K_p: ' + str(p) + ', K_i: ' + str(i))
        
            
        
            
if __name__ == '__main__':
    
    #Encoder A
    encAPin1 = pyb.Pin(pyb.Pin.cpu.B6) #Timer4, channel 1
    encAPin2 = pyb.Pin(pyb.Pin.cpu.B7) #Timer4, channel 2
    tim4 = pyb.Timer(4, period = 65535, prescaler = 0) #Encoder A, PB6 and PB7, timer 4
    encoderA = Encoder(encAPin1, encAPin2, tim4)
    
    #Motor A
    tim3 = pyb.Timer(3, freq = 20000);
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, mode=pyb.Pin.OUT_PP, value=1);
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5);
    motorA = Motor(pin_nSLEEP, pin_IN1, 1, pin_IN2, 2, tim3)
    
    #PIDA
    #p = 50, i = 3
    kpA = 50
    kiA = 1
    kdA = 0
    pidA = ClosedLoop(kp = kpA, ki = kiA, kd = kdA, sat = 75)
    
    #ControllerA
    controllerA = Controller(motorA, encoderA, pidA)
    
    
    
    #Encoder B
    encBPin1 = pyb.Pin(pyb.Pin.cpu.C6)
    encBPin2 = pyb.Pin(pyb.Pin.cpu.C7)
    tim8 = pyb.Timer(8, period = 65535, prescaler = 0) #Encoder B, PC6 and PC7, timer 4
    encoderB = Encoder(encBPin1, encBPin2, tim8)
    
    #Motor B
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)
    motorB = Motor(pin_nSLEEP, pin_IN3, 3, pin_IN4, 4, tim3)
    
    #PIDB
    kpB = 50
    kiB = 1
    kdB = 0
    pidB = ClosedLoop(kp = kpB, ki = kiB, kd = kdB, sat = 75)
    
    #ControllerB
    controllerB = Controller(motorB, encoderB, pidB)
    
    prevTime = utime.ticks_ms()
    #controllerB.selfTune(3, 10, pIn = 20, iIn = 0.5)
    while not controllerB.exit:
        if utime.ticks_diff(utime.ticks_ms(), prevTime) > 20:
            controllerB.runStateMachine()
            prevTime = utime.ticks_ms()
    
    
    
    