'''
@file nucleoMain.py
@brief A script to initialize all objects on the Nucleo side and update their state machines regularly.
@details nucleoMain.py initializes all of the pyb pins connected to hardware and encoderDriver.Encoder, motorDriver.Motor, as well as controller.Controller
objects for controlling both of the motors. It also initializes \link shares.py shares.py \endlink and a nucleoSerial.nucleoSerial object for communication
between tasks and the PC UI. Then, the main script runs the controller task, controller.Controller, state machine every 20 ms and the serial coms
task, nucleoSerial.nucleoSerial state machine as fast as possible.
Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Final%20Project/nucleoMain.py
@author Eliot Briefer
@ingroup week4
'''

'''
@defgroup week1 Week One
@defgroup week2 Week Two
@defgroup week3 Week Three
@defgroup week4 Week Four
'''

import pyb, utime, shares
from nucleoSerial import nucleoSerial
from encoderDriver import Encoder
from motorDriver import Motor
from pid import ClosedLoop
from controller import Controller

if __name__ == '__main__':
    pyb.repl_uart(None)
    #Encoder A
    encAPin1 = pyb.Pin(pyb.Pin.cpu.B6) #Timer4, channel 1
    encAPin2 = pyb.Pin(pyb.Pin.cpu.B7) #Timer4, channel 2
    tim4 = pyb.Timer(4, period = 65535, prescaler = 0) #Encoder A, PB6 and PB7, timer 4
    ##@brief The Encoder object for encoder A, connected to pins PB6 and PB7.
    encoderA = Encoder(encAPin1, encAPin2, tim4)
    
    #Motor A
    tim3 = pyb.Timer(3, freq = 20000);
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, mode=pyb.Pin.OUT_PP, value=1);
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5);
    ##@brief The Motor object for motor A, connected to pins PB4 and PB5.
    motorA = Motor(pin_nSLEEP, pin_IN1, 1, pin_IN2, 2, tim3)
    
    #PIDA
    ##@brief The Kp value for motor A.
    kpA = 50
    ##@brief The Ki value for motor A.
    kiA = 1
    ##@brief The Kd value for motor A.
    kdA = 0
    ##@brief The ClosedLoop object for calculating PID response for controllerA
    pidA = ClosedLoop(kp = kpA, ki = kiA, kd = kdA, sat = 75)
    
    #ControllerA
    ##@brief The Controller task to control motor A.
    controllerA = Controller(motorA, encoderA, pidA)
    
    
    
    #Encoder B
    encBPin1 = pyb.Pin(pyb.Pin.cpu.C6)
    encBPin2 = pyb.Pin(pyb.Pin.cpu.C7)
    tim8 = pyb.Timer(8, period = 65535, prescaler = 0) #Encoder B, PC6 and PC7, timer 4
    ##@brief The Encoder object for encoder B, connected to pins PC6 and PC7.
    encoderB = Encoder(encBPin1, encBPin2, tim8)
    
    #Motor B
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0)
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1)
    ##@brief The Motor object for motor B, connected to pins PB0 and PB1.
    motorB = Motor(pin_nSLEEP, pin_IN3, 3, pin_IN4, 4, tim3)
    
    #PIDB
    ##@brief The Kp value for motor B. 50
    kpB = 50
    ##@brief The Ki value for motor B. 1.5
    kiB = 1.5
    ##@brief The Kd value for motor B. 10
    kdB = 0
    ##@brief The ClosedLoop object for calculating PID response for controllerB
    pidB = ClosedLoop(kp = kpB, ki = kiB, kd = kdB, sat = 75)
    
    #ControllerB
    ##@brief The Controller task to control motor B.
    controllerB = Controller(motorB, encoderB, pidB)
    
    #Serial Coms
    ##@brief The nucleoSerial task for handling serial communications with the PC UI.
    ser = nucleoSerial()
    
    ##@brief The last time the task loop ran, in ms from startup.
    prevTime = utime.ticks_ms()
    while True:
        if utime.ticks_diff(utime.ticks_ms(), prevTime) > 20:
            controllerB.runStateMachine()
            prevTime = utime.ticks_ms()
        ser.runStateMachine()
        

