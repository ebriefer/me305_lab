'''
@file csvResampler.py
@brief a script to convert between .csv files and python List objects, and resample time based .csv files at a different frequency.
@details This script is used to create downsampled .csv reference motion profiles to be read
by the controller task running on the Nucleo, controller.Controller. This script does not
run in sequence with any of the other nucleo or user interface tasks, and is designed only for pre-processing
the reference profile data before copying to the Nucleo.

Although this script was only used to downsample one very specific reference profile, it is designed to
be general, and can up or downsample any .csv file with at least one ordered time column and any number of rows and columns
using linear interpolation.
Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Final%20Project/csvResampler.py
@ingroup week4
@author Eliot Briefer
'''


#CSV Resampler
from numpy import arange

def resampleCSV(file, newFile, timeCol = 0, timeStep = 0.1):
    '''
    @brief Resample a time based .csv file at a different frequency.
    @param file The File object to read from, should be a .csv file.
    @param newFile The File object to write resampled data to.
    @param timeCol The column in file that contains times.
    @param timeStep The new time step to sample the data at.
    '''
    
    ## @brief A nested List of the input .csv file. Format is [[row1], [row2], ...]
    data = readCSV(file)
    print(data)
    ## @brief The first time in the input data.
    startTime = data[0][timeCol]
    print(startTime)
    ## @brief The last time in the input data.
    endTime = data[-1][timeCol]
    print(endTime)
    ## @brief A range type object containing the new timesteps to sample at.
    timeSteps = arange(startTime, endTime + timeStep, timeStep)
    ## @brief A nested list of the resampled data.
    newData = []
    for t in timeSteps:
        newData.append(searchInterp(data, t, searchCol = timeCol))
        
    print(newData)
    writeCSV(newFile, newData)
    file.close()
    newFile.close()
                
def writeCSV(file, data):
    '''
    @brief A method to write a nested List object to a .csv file.
    @param file The File object to write to.
    @param data The nested list to convert to a .csv file.
    '''
    for line in data:
        csvLine = ''
        for word in line:
            if csvLine == '':
                csvLine += str(word)
            else:
                csvLine += ', ' + str(word)
        file.write(csvLine + '\n')
    
def readCSV(file):
    '''
    @brief A method to load a .csv into a nested List object.
    @param file The file object to read from.
    @return A nested List object containing the data from the .csv file.
    '''
    data = []
    lines = file.readlines()
    for line in lines:
        data.append([float(word.strip()) for word in line.split(',')])
    return data

def searchInterp(data, target, searchCol = 0):
    '''
    @brief A method to search a nestested List object for a given value using interpolation.
    @param data The nested List object to search through. Format should be [[row1], [row2],...].
    @param target The target value to search for.
    @param searchCol The column of the nested List to search in.
    @return the linearly interpolated row list at the target value.
    '''
    #search a list along a given column with interpolation
    #data is [[a,b,...],...]
    value = []
    foundValue = False
    for r, row in enumerate(data):
        if (row[searchCol] > target  or r == len(data) - 1) and not foundValue:
            foundValue = True
            for c, col in enumerate(row):
                if c == searchCol:
                    value.append(target)
                else:
                    x1 = 0
                    y1 = 0
                    x2 = 0
                    y2 = 0
                    if r == 0:
                        x1 = data[r+1][searchCol]
                        y1 = data[r+1][c]
                        x2 = data[r][searchCol]
                        y2 = col
                    else:
                        x1 = data[r-1][searchCol]
                        y1 = data[r-1][c]
                        x2 = data[r][searchCol]
                        y2 = col
                    value.append(linInterp(target, x1, y1, x2, y2))
    return value

def linInterp(x, x1, y1, x2, y2):
    '''
    @brief A method to linearly interpolate between two points.
    @param x The target x value.
    @param x1 The x value of point 1.
    @param y1 The y value of point 1.
    @param x2 The x value of point 2.
    @param y2 The y value of point 2.
    @return the linearly interpolated y value at x.
    '''
    return y1 + (x-x1)*(y2-y1)/(x2-x1)

if __name__ == '__main__':
    file = open('reference.csv')
    newFile = open('rescaledReference.csv', 'w')
    
    resampleCSV(file, newFile, timeStep = 0.1)