'''
@file motorDriver.py
@brief A class to power a brushed, dc motor using PWM.
@details The motorDriver.Motor class is used by the controller task, controller.Controller
to set the duty cycle of the motor using PWM.
Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Final%20Project/motorDriver.py
@author Eliot Briefer
@ingroup week3
'''

import pyb

class Motor:
    '''
    @brief The Motor class sets the power of a brushed, dc motor using PWM
    @details The motorDriver.Motor class is used by the controller task, controller.Controller
    to set the duty cycle of the motor using PWM.
    Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Final%20Project/motorDriver.py
    '''
    def __init__(self, nSLEEP_pin, IN1_pin, IN1_ch, IN2_pin, IN2_ch, timer):
        '''
        @brief A method to initialize a Motor object.
        @param nSLEEP_pin The pyb.Pin object that enables and diables the motor.
        @param IN1_pin The pyb.Pin object connected to one lead of the motor.
        @param IN2_pin The pyb.Pin object connected to the other lead of the motor.
        @param IN1_ch The channel number of the PWM timer corresponding to IN1_pin.
        @param IN2_ch The channel number of the PWM timer corresponding to IN2_pin.
        @param timer The pyb.Timer object used to generate PWM for the motor.
        '''
        pyb.repl_uart(None)
        ##@brief The pyb.Pin object that enables and diables the motor.
        self.nSLEEP = nSLEEP_pin
        ##@brief The pyb.Timer object used to generate PWM for the motor.
        self.timer = timer
        ##@brief The channel of the PWM timer corresponding to the first motor lead.
        self.IN1 = self.timer.channel(IN1_ch, pyb.Timer.PWM, pin=IN1_pin)
        ##@brief The channel of the PWM timer corresponding to the second motor lead.
        self.IN2 = self.timer.channel(IN2_ch, pyb.Timer.PWM, pin=IN2_pin)
        #print('Creating a motor driver')
        
    def enable(self):
        '''
        @brief A method to enable the motor by pulling the sleep pin high.
        '''
        self.nSLEEP.value(1)
        #print('Enabling motor')
    
    def disable(self):
        '''
        @brief A method to disable the motor by pulling the sleep pin low.
        '''
        self.nSLEEP.value(0)
        #print('Disabling motor')
    
    def set_duty(self, duty):
        '''
        @brief A method to set the duty cycle of the motor.
        @param duty The duty cycle to set the motor to, from -100 to 100.
        '''
        if duty >= 0:
            self.IN1.pulse_width_percent(duty)
            self.IN2.pulse_width_percent(0)
            #print('Set duty cycle to: ' + str(duty))
        else:
            self.IN1.pulse_width_percent(0)
            self.IN2.pulse_width_percent(-1* duty)
            #print('Set duty cycle to: ' + str(duty))
            
    
        

if __name__ == '__main__':
    
    #print(pyb.Pin.dict())
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, mode=pyb.Pin.OUT_PP, value=1);
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5);
    pin_IN3 = pyb.Pin(pyb.Pin.cpu.B0);
    pin_IN4 = pyb.Pin(pyb.Pin.cpu.B1);
    tim3 = pyb.Timer(3, freq = 20000);
    motor1 = Motor(pin_nSLEEP, pin_IN1, 1, pin_IN2, 2, tim3)
    motor2 = Motor(pin_nSLEEP, pin_IN3, 3, pin_IN4, 4, tim3)
    
    motor1.enable()
    motor1.set_duty(70)
    pyb.delay(2000)
    motor1.set_duty(-70)
    pyb.delay(2000)
    motor1.set_duty(0)
    motor2.enable()
    motor2.set_duty(70)
    pyb.delay(2000)
    motor2.set_duty(-70)
    pyb.delay(2000)
    motor2.set_duty(0)
    motor1.enable()
    motor1.set_duty(90)
    pyb.delay(2000)
    motor1.set_duty(-90)
    pyb.delay(2000)
    motor1.set_duty(0)
    motor2.enable()
    motor2.set_duty(90)
    pyb.delay(2000)
    motor2.set_duty(-90)
    pyb.delay(2000)
    motor2.set_duty(0)