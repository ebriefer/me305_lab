'''
@file nucleoSerial.py
@brief A class for handling serial communications with the PC UI.
@details The nucleoSerial.nucleoSerial class is a state machine to listen
for user input over serial, and to check and update values in \link shares.py shares.py \endlink
to communicate with the controller task, controller.Controller
Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Final%20Project/nucleoSerial.py
@author Eliot Briefer
@ingroup week1
'''
import utime, array, pyb, shares
from math import e, pi, sin

class nucleoSerial:
    '''
    @brief A class handles all serial communication with the PC.
    @details The nucleoSerial class is a task that listens for ASCII characters
    from the PC, writes position data to serial, and updates values in shares.py
    to interact with the controller tasks.
    '''
    ## @brief A Dictionary of messages to display at each state change.
    stateMessages = {
        0 : "NUCLEO: Waiting for serial input.",
        1 : "NUCLEO: collecting data.",
        2 : "NUCLEO: Transmiting data."
        }
    
    def __init__(self, startState = 0, batchSize = 10):
        '''
        @brief A method to initialise a nucleoSerial object.
        @param startState The state to start the serial coms task state machine in.
        @param batchSize The chunk size to send data over serial in, in lines.
        '''
        pyb.repl_uart(None)
        ##@brief The chunk size to send data over serial in, in lines.
        #@details nucleoSerial objects are meant to be called as fast as possible,
        #so small (<100) batch sizes are prefered in order to prevent memory allocation errors
        self.batchSize = batchSize
        ##@brief pyb.UART object for handling serial communications.
        self.myuart = pyb.UART(2)
        ##@brief The state to start the serial coms task state machine in.
        self.state = startState
        ##@brief The last time (in ms from startup) that the state machine transitioned states.
        self.lastTransition = utime.ticks_ms()
        ##@brief A nested List of values to write to serial. Format is [[row1], [row2], ...]
        self.serVal = [[] for _ in range(batchSize+1)]
        #@brief The current row number being set or read in self.serVal
        self.currentIndx = 0
        
        
    def runStateMachine(self):
        '''
        @brief A method to run through the serial coms state machine logic once.
        '''
        ##@brief The time since last state transition, in floating point seconds.
        t = utime.ticks_diff(utime.ticks_ms, self.lastTransition) / 1000
        if self.state == 0:
            shares.collectData = False
            if self.myuart.any() != 0:
                val = self.myuart.readchar()
                #if val == 103: 
                self.transitionStates(1)
                self.serVal = [[] for _ in range(self.batchSize+1)]
                shares.collectData = True
                self.currentIndx = 0
                
        elif self.state == 1:
            if self.currentIndx >= self.batchSize or not shares.collectData: 
                self.transitionStates(2)
                self.currentIndx = 0
                print(self.serVal)
            elif self.myuart.any() != 0:
                val = self.myuart.readchar()
                if val == 115: 
                    self.transitionStates(0)
            else:
                dataPoint = shares.getMotorState()
                if dataPoint is not None:
                    self.serVal[self.currentIndx] = dataPoint
                    print(self.serVal[self.currentIndx])
                    self.currentIndx += 1
                #self.transitionStates(1)
                
                
        elif self.state == 2:
            if self.currentIndx >= self.batchSize:
                if shares.collectData:
                    self.transitionStates(1)
                else:
                    self.transitionStates(0)
                self.currentIndx = 0
            elif self.myuart.any() != 0:
                val = self.myuart.readchar()
                if val == 115: 
                    self.transitionStates(0)
            elif t >= 0.01:
                self.sendData(self.currentIndx)
                self.currentIndx += 1
                #self.transitionStates(2)
                
        
    def genData(self, tick):
        '''
        @brief A debugging method to imitate data from an encoder.
        @details genData calculates the value of a decaying sin wave at a given time.
        @param tick The time, in ms.
        @return A tupel of the form (time t, (seconds), f(t))
        '''
        ##@brief The desired time, in floating point seconds.
        t = utime.ticks_diff(tick, self.lastTransition) / 1000
        return (t, e**((-1/10)*t)*sin(2*pi/3)*t)
    
    def sendData(self, i):
        '''
        @brief A method to write all of the data in self.serVal to serial line by line as comma seperated values.
        '''
        ##@brief the current csv row being written to serial
        row = ''
        for word in self.serVal[i]:
            if row == '':
                row += str(word)
            else:
                row += ', ' + str(word)
                
        print('Sending: ' + row)
        self.myuart.write(row + '\r\n')
    
    def transitionStates(self, st, customMessage = ""):
        '''
        @brief A method that changes the state machine state, self.state and prints the state transition message.
        @param st The state to transition to.
        @customMessage An additional message to print before the state transition message.
        '''
        self.state = st
        if customMessage != "": print(customMessage)
        print(self.stateMessages[st])
        self.lastTransition = utime.ticks_ms()

