'''
@file encoderDriver.py
@brief A class to read values from a quadrature encoder.
@details The encoderDriver.Encoder class is used by the controller task, controller.Controller
to get closed loop feedback of the motor's position and velocity.
Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Final%20Project/encoderDriver.py
@ingroup week2
@author Eliot Briefer
'''

import pyb
from math import pi

class Encoder:
    '''
    @brief The Encoder class reads ticks from a quadrature encoder and converts them to the shaft position in rotations.
    '''
    def __init__(self, IN1_pin, IN2_pin, timer, ticksPerRev = 4000):
        '''
        @brief Method to initialise an Encoder object.
        @param IN1_pin The pyb.Pin object connected to channel one of the encoder.
        @param IN2_pin The pyb.Pin object connected to channel two of the encoder.
        @param timer The pyb.Timer object to read from the input pins.
        @param ticksPerRev The total number of timer ticks in a revolution. Include both channels' rising and falling edges.
        '''
        ##@brief the pyb.Timer object to read from the input pins
        self.timer = timer
        ##@brief The total number of timer ticks in a revolution. Include both channels' rising and falling edges.
        self.ticksPerRev = ticksPerRev
        ##@brief The first channel of the encoder.
        self.CH1 = self.timer.channel(1, pyb.Timer.ENC_A, pin=IN1_pin)
        ##@brief The seccond channel of the encoder.
        self.CH2 = self.timer.channel(2, pyb.Timer.ENC_B, pin=IN2_pin)
        ##@brief The current recorded position of the encoder, in rotations from initialization.
        self.position = 0
        ##@brief The previous position of the encoder, in rotations from initialization.
        self.prevPosition = 0
        ##@brief The previous tick count of encoder channel 1, self.CH1.
        self.prevTick1 = 0
        ##@brief The previous tick count of encoder channel 2, self.CH2.
        self.prevTick2 = 0
        ##@brief The last position printed to the console for debugging.
        self.lastPrintedPostion = 0
        
        
    def update(self):
        '''
        @brief Method to update the current position of the encoder.
        '''
        tick1 = self.CH1.capture()
        tick2 = self.CH2.capture()
        dif1 = tick1 - self.prevTick1
        dif2 = tick2 - self.prevTick2
        
        #Detect underflow
        if dif1 > 65535/2:
            dif1 -= 65535
            #print("1 Underflowed")
        elif dif1 < -1 * 65535/2:
            dif1 += 65535
            #print("1 Overflowed")
        
        if dif2 > 65535/2:
            dif2 -= 65535
            #print("2 Underflowed")
        elif dif2 < -1 * 65535/2:
            dif2 += 65535
            #print("2 Overflowed")
        
        dif = dif1 + dif2
        
        
        self.prevTick1 = tick1
        self.prevTick2 = tick2
        self.prevPosition = self.position
        self.position += dif / self.ticksPerRev
        
        if abs(self.position - self.lastPrintedPostion) > 0.1:
            #print("Position: " + str(self.position))
            self.lastPrintedPostion = self.position
    
    def get_position(self):
        '''
        @brief Method to get the last read position of the encoder.
        @return The last read position of the encoder, in rotations.
        '''
        return self.position
    
    def get_radians(self):
        '''
        @brief Method to get the last read position of the encoder.
        @return The last read position of the encoder, in radians.
        '''
        return self.position * (2*pi)
    
    def get_degrees(self):
        '''
        @brief Method to get the last read position of the encoder.
        @return The last read position of the encoder, in degrees.
        '''
        return self.position * 360
    
    def set_position(self, pos = 0):
        '''
        @brief Method to reset the position of the encoder to a given value.
        @param pos The position to set the encoder position to, in rotations.
        '''
        self.position = pos
    
    def get_delta(self):
        '''
        @brief Method to get the difference between the last two position readings.
        @return The difference of the last two position readings, in rotations.
        '''
        return self.position - self.prevPosition
        
if __name__ == "__main__":
    
    #Encoder A
    encAPin1 = pyb.Pin(pyb.Pin.cpu.B6) #Timer4, channel 1
    encAPin2 = pyb.Pin(pyb.Pin.cpu.B7) #Timer4, channel 2
    tim4 = pyb.Timer(4, period = 65535, prescaler = 0) #Encoder A, PB6 and PB7, timer 4
    
    #Encoder B
    pin_T3_CH1 = pyb.Pin(pyb.Pin.cpu.C6)
    in_T3_CH2 = pyb.Pin(pyb.Pin.cpu.C7)
    tim3 = pyb.Timer(3, period = 65535, prescaler = 0) #Encoder A, PB6 and PB7, timer 4
    
    encA = Encoder(encAPin1, encAPin2, tim4)
    
    while True:
        encA.update()