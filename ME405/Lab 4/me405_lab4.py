'''
@file me405_lab4.py
@brief This script uses I2C communication to measure ambient
temperature with a MCP9808 microchip.
@details The me405_lab4.MCP9808 class provides methods for communicating
with the MCP9808 chip for measuring the temperature in both Celcius and
Farenheit. This script provides an example use of the me405_lab4.MCP9808
to measure the ambient temperature at one minute intervals and record them
to a .csv file on the Nucleo.\n
Link to source code:
@author Eliot Briefer
@date 5/13/2021
'''
import pyb, utime


class MCP9808:
    '''
    @brief A class to communicate with the MCP9808 microchip using I2C
    communication.
    '''
    def __init__(self, i2c, address, manuf_id = 0b01010100):
        '''
        @brief A method to initialize an MCP9808 object.
        @param i2c A pyb.I2C object to use for communicating with the MCP9808.
        @param address The i2c address of the MCP9808 address.
        @param manuf_id The manufacturer id of the MCP9808 chip, used to check
        communication with the MCP9808 chip using the me405_lab4.MCP9808.check method.
        '''
        
        ##@brief A pyb.I2C object to use for communicating with the MCP9808.
        self.i2c = i2c
        ##@brief The i2c address of the MCP9808 address.
        self.addr = address
        ##@brief The manufacturer id of the MCP9808 chip
        self.id = manuf_id
            
       
    def check(self):
        '''
        @brief A method to check that the MCP9808 is properly communicating
        with the Nucleo.
        @return Wether or not the MCP9808 is is properly communicating
        with the Nucleo (True = communication established).
        '''
        manuf_id = self.i2c.mem_read(2, self.addr, 6)
        if manuf_id == self.id:
            return True
        else: return False
        
    def celsius(self):
        '''
        @brief A method to get the temperature in Celcius from the MCP9808.
        @return The ambient temperature in Celcius.
        '''
        tempB = self.i2c.mem_read(2, self.addr, 8)
        upper = tempB[0:1] & 0x1F
        lower = tempB[1:2]
        if upper & 0x10 == 0x10:
            upper = upper & 0x0F
            return 256 - (upper*16 + lower/16)
        else:
            return upper*16 + lower/16
        
    def fahrenheit(self):
        '''
        @brief A method to get the temperature in Fahrenheit from the MCP9808.
        @return The ambient temperature in Fahrenheit.
        '''
        tempC = self.celcius
        return tempC * 1.8 + 32
    
    def collect(self, sample_time, num_samples, scale = 'C'):
        '''
        @brief A method to collect a series of temperature readings over time.
        @param sample_time The time between samples, in milli seconds
        @param durration The number of samples to record
        @return A floating point array of [[time_stamp, temperature]]
        '''
        pass
    
if __name__ == '__main__':
    i2c = pyb.I2C(1)
    i2c.init(pyb.I2C.MASTER, baudrate=100000)
    tempSensor = MCP9808(i2c, 0x18)
    utime.sleep(1) #Allow temp sensor to boot
    if not tempSensor.check():
        print('No temperature sensor detected')
    else:
        startTime = utime.ticks_ms()
        while True:
            utime.sleep(60)
            