##\mainpage MECHATRONICS DOCUMENTATION
#
#By Eliot Briefer \n
#\n
#This website provides documentation for code written for Calpoly’s Mechatronics courses.
#All code is written in python, using the micropython library to run on
#a Nucleo micro controller.
#\n
#\n
#Courses:
# - \subpage me305
# - \subpage me405

##\page me405 ME405: MECHATRONICS
#This page contains all documentation for Calpoly's ME405 mechatronics class.
# Included modules are:
#  * Lab0x01 (\ref sec_lab01)
#  * Lab0x02 (\ref sec_lab02)
#  * Lab0x03 (\ref sec_lab03)
#  * Term Project (\ref sec_term_project)
#
#  @section sec_lab01 Lab0x01 Documentation
#  * Source: https://bitbucket.org/ebriefer/me305_lab/src/master/ME405/Lab%200/vendotron.py
#  * Documentation: vendotron.py
#  * Video: https://youtu.be/L5GUWKcDoz8
#
#  @section sec_lab02 Lab0x02 Documentation
#  * Source: https://bitbucket.org/ebriefer/me305_lab/src/master/ME405/Lab%202/thinkFast.py
#  * Documentaion: thinkFast.py
#  * \subpage me405_lab02 "Write up"
#
#  @section sec_lab03 Lab0x03 Documentation
#  * Source: https://bitbucket.org/ebriefer/me305_lab/src/master/ME405/Lab%203/
#  * Documentation:
#    * User interface: me405_lab3_front_end.py
#    * Data collection task me405_lab3.py
#    .
#  * \subpage me405_lab03 "Write up"
#
#  @section sec_term_project Term Project
#  * \subpage systemModel "System Modeling"
#
#  @author Eliot Briefer
#
#  @date Apr. 22, 2021

##\page me405_lab03 ME405 LAB0x03: Analog to Digital Converters and Serial Communications
#
#\n
#
#
#

##\page me405_lab02 ME405 LAB0x02: Reaction Time Measuring
#
#\n
#The goal of this lab was to use interrupts on the Nucleo to accuratley measure the user's reaction time.
#Specifically, the Nucleo waits a random amount of time between 2 and 3 seconds, then turns the green built
#in LED on. The user must then press the blue "User" button on the Nucleo as fast as possible and the program
#will measure their reaction time. This process repeates until the user presses ctrl+c to exit the program,
#at which point the Nucleo prints out their average reaction time and the number of attempts they did. If
#there were no recorded attempts the Nucleo will print out an error message instead.
#\n
#\n
#For my implementation of this project I chose to use a finite state machine that switches between 3 states:
# - state 0: led off and waiting
# - state 1: led on for one second
# - state 2: updating average reaction time and reseting values
#
#Note that the users reaction time is measured using an interrupt, which can occur at any point in the FSM. However,
#to avoid incorrect readings, I have configured the interrupt to only record the users reaction time if it is the first time they have pressed the
#button after the led has turned on (ie the FSM is in state 1).
#\n
#\n
#I implemented the reaction time measuring system in two different ways. The first was using utime.ticks_us() and utime.ticks_diff()
#To record the users reaction time when they pressed the button. The second method used the built in timer modules in micro python to
#both turn the led on and off and record when the user presses the button.

##\page systemModel TERM PROJECT: System Modeling and Kinematics
#\image html ME405HW3.jpg
#\image html ME405HW3_1.jpg
#\image html ME405HW3_2.jpg
#\image html ME405HW3_3.jpg
#\image html ME405HW3_4.jpg

##\page me305 ME305: INTRODUCTION TO MECHATRONICS
#
#\n
#This website provides documentation for code written for Calpoly’s ME305 Introduction to
#Mechatronics course. All code is written in python, using the micropython library to run on
#a Nucleo micro controller.
#The projects for this course are:
# - \subpage finalproject "Final Project" Closed loop motor control.
# - \subpage lab1 "Lab 1" Fibonacci sequence generation.
# - \subpage lab2 "Lab 2" Blinking LEDs.
# - \subpage lab3 "Lab 3" Simon says game.

## \page lab1 LAB 1: GENERATING THE FIBONACCI SEQUENCE
#The goal of this lab was to create a python script to generate the Fibonacci sequence.
#See the \link Lab1.py Lab 1 script \endlink documentation for more details.
# - Source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Lab%201/Lab1.py

## \page lab2 LAB 2: CONTROLLING LEDs
#The goal of this lab was to program a \link lab2.py state machine \endlink to blink an LED on the Nucleo
#in different patterns.
# - Source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Lab%202/lab2.py
# - Demonstration video: https://youtu.be/98HGiMh8p0U

## \page lab3 LAB 3: SIMON SAYS GAME
#The goal of this lab was to create a state machine that would play Simon says with the
#user using an LED on the Nucleo and the blue user input button.\link lab3.py Lab 3 \endlink
#implements a state machine that controls the LED on the Nucleo and listens for presses of the 
#blue user button on the Nucleo. It also prints messages to the console to direct the user
#how to play Simon says. See the lab3.simonSays class documentation for more information.
# - Source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Lab%203/lab3.py
# - Demonstration video: https://youtu.be/LWRrAM0yLX0

## \page finalproject CLOSED LOOP MOTOR CONTROL AND MOTION PROFILE TRACKING
#
#As the final project for Calpoly's Intro to Mechatronics class I created a
#set of drivers and tasks to control the position and velocity of a brushed DC
#motor using closed loop feedback and a PID controller. The motor is controlled
#with a Nucleo board and programed using micropython.
#
#The documentation for this project is divided into four weeks:
# - \subpage week1 "Week 1" serial communication.
# - \subpage week2 "Week 2" encoder feedback.
# - \subpage week3 "Week 3" closed loop motor control.
# - \subpage week4 "Week 4" motion profile tracking and full system integration.
#
# - Demonstraion video: https://youtu.be/s3JvVQnaGNg
# 
#The final goal of this project was to have a motor track a reference position
#and velocity profile and send the actual position and velocity back to a
#connected PC via serial for plotting.\n 
#
#More specifically, the Nucleo receives closed loop feedback #from an encoder connected to the motor by a belt and pulley, which is decoded
#using the encoder driver created in \ref week2 "week 2". The closed loop feedback
#is then fed to a controller task, created in \ref week3 "week 3" and \ref week4 "week 4",
#which determines how the motor should be powered to track a reference profile.
#The controller output is then fed to a motor driver, created in \ref week2 "week 2",
#which controls the motor power using PWM. Finally, a serial communication task created in weeks \ref week1 "one"
#and \ref week4 "four", sends the actual motor position and velocity to a connected PC
#over serial for plotting.\n
#
#There was significant friction in the pulleys that caused the calculated proportional gain
#of the controller to be far to low for the real life system, as discussed in \ref week3 "week 3". To improve the perforamnce of
#the PID controler, I created a method, controller.Controller.selfTune(), which repeatedly
#subjects the motor to a step input and incrementally varries Kp and Ki to minimize the J value
#of the controller. Note that Kp and Ki here is proportional gain relative to the position error,
#not the velocity error as previously calculated. For the full PID controller taking the position
#error signal as an input, as implemented in \ref week4 "week 4" Kd is actually equivalent to the
#kp for the P controller acting on a velocity error signal, as implemented in \ref week3 "week 3".\n
#
#The controller.Controller.selfTune() only tunes P and I values, wich results in a controler
#that performs only position control, not velocity control. The results of this PI controler
#are shown in figures 1 and 2 below.
#
#\image html PIposition.png
#\image html PIvelocity.png
#
#Based on the results show above, PI control is capable of
#tracking a position profile to a reasonable degree of accuracy;
#However, it's transient response is not smooth, which results
#in extremely poor velocity profile tracking. The total J value,
#a measure of how well the controller performs, was 6.5 (lower numbers are better).
#To improve the velocity tracking performance a full PID controller is needed,
#which can perform simultaneous position and velocity control. 
#To create an effective PID controller, I added back in a Kd gain of 10 V/(rotation per second),
#which I experimentally found to be the best Kd value in \ref week3 "week 3".
#The results of this full PID controler are shown in Figures 2  and 3 below.
#
#\image html P50I1.5D10position.png
#\image html P50I1.5D10velocity.png
#
#The full PID controller's transient response is much smoother, as seen by the
#smaller fluctuations in the velocity graph, while still maintaining accurate position control, 
#something which is not possible with a velocity controller only. The J value of the full PID
#controller was 3.16, less than half that of the J value of the PI controller,
#which confirms that the PID controller is much more effective at acuratley controlling
#both position and velocity.


## \page week1 SERIAL COMMUNICATION
#
#Data is transmitted between the Nucleo and a connected PC using serial communication.
#There are two tasks to perform serial communication, \link FrontEnd.py Front End \endlink which is a python script
#that runs on the PC, and nucleoSerial.NucleoSerial which is a task running on the Nucleo. FrontEnd
#prompts the user to send single ASCII characters to the Nucleo and listens for
#motor position data sent from the Nucleo. nuceloSerial listens for ASCII characters
#from the PC and sends motor position data to the PC while the Nucleo is performing
#motion profile tracking.\n
#
#nuceloSerial.NucleoSerial is set up as a state machine to allow for rapid execution on the single
#threaded Nucleo without interrupting other tasks. The state machine cycles between listening
#for user input, logging motor position data, and transmitting motor position data, as
#shown in Figure 1 below.
#
#\image html sercomsshort.jpg
#
#nucleoSerial gets motor position data from the controller task via \link shares.py shares \endlink, which is used
#to share data between tasks on the Nucleo.

## \page week3 DC MOTOR CONTROL
# 
#To interface with the motor I created a motor driver class, motorDriver.Motor which
#uses PWM applied to both leads of the motor to control the voltage to the motor.
#The motor class is used by the controller task, controller.Controller to set the duty 
#cycle of the motor.\n
#
#Additionally, I created a proportional velocity controller (the D in PID) to control the motor's velocity.
#pid.closedLoop is a more advanced version of this controller that I developed in \ref week4 "week 4".
#The proportional velocity controller uses a single proportional gain, Kp to turn the velocity error
#signal (desired - measured velocity) into a motor duty cycle (in percent). Note that because
#this controller was performing velocity control, and the PID controller I created in \ref week4 "week 4"
#was performing position control the Kp value in this section is actually equivalent to the Kd value
#in \ref week4 "week 4".\n
#
#To find a starting value for Kp, I used the transfer function for a DC motor and solved for a Kp that
#would result in a steady state velocity error of less 5 percent. The calculations are shown in figure 1 below.
#
#\image html kpCalcs.jpg
#
#One of the major assumptions I made in these calculations was that there was no damping in the system.
#This assumption is not very good because the belt that connects the motor to the encoder creates so much friction
#That it stalls the motors above 75 percent duty cycle; However, I did not have a good way to measure the friction.
#These calculations yielded a Kp value of 0.001522 %/rpm for 5 percent steady state error.\n
#
#As expected, when I plugged this gain value into my controler and subjected it to a step input of 0 to 300 RPM,
#the gain was so low that the motor couldn't overcome the friction of the belt and never moved, shown in Figure 2 below.
#
#\image html PVcalc.png
#
#Next, I arbitrarily increased the the gain to 0.167 %/RPM (10 %/RPS, which is what my pid.ClosedLoop measures in).
#This was a large enough gain that the motor was able to nearly reach the target velocity of 300 RPM; However, due to the fact
#that the belt friction changes dramatically through one revolution, the system never came close to steady state because
#the friction torque the motor had to overcome was constant changing. The results of the step input with a gain of 0.167 V/RPM
#are shown below in Figure 3.
#
#\image html PV10.png
#
#After trying a gain of 0.33 %/RPM (20 %/RPS), shown in Figure 4 below, I settled upon a gain of 0.2 %/RPM (12 %/PRS).
#
#\image html PV20.png
#
#At 0.2 %/RPM, the motor averaged a speed near the target velocity of 300 RPM, shown below, which was the best I could hope for given the
#unsteady loading on the motor.
#
#\image html PV12.png
#
#Although I would have liked to use the controller J values to compare them, the motor speed
#was fluctuating so much that even between runs of the same controller the J values would fluctuate by
#nearly an order of magnitude. For reference, the J values of the best controller with a gain of 0.2 %/RPM
#were arround 500, while the J value for the best PID controler I made in \ref week4 "week 4" was
#3.16.

## \page week2 ENCODER DRIVER
#
#The motor is connected to a quadrature encoder to get position and velocity feedback.
#To decode the signal from the encoder I created an encoder driver class, encoderDriver.Encoder
#which reads the position of the encoder in rotations from startup as well as other
#common functions of an encoder. The controller task controller.Controller uses the encoder driver
#to get closed loop position and velocity feedback from the motor. To facilitate debugging the decoder,
#I added commands to the serial coms task, nucleoserial.NucleoSerial to allow the user to reset the encoder
#position to zero, as well as print out the position and position change of the encoder. The state machine diagram
#is shown below.
#
#\image html sercoms.jpg
#
#Due to the complexity of this state machine, I later simplified the state machine
#to just collect motor position data when the controller task, controller.Controller was collecting data
#and transmit it back to the PC via serial. This simplified state machine is shown in \ref week1 "week 1".

## \page week4 MOTION PROFILE TRACKING AND CONTROLLER TASK
#
#To control the motor position and velocity using closed loop feedback I created a
#PID controller, pid.ClosedLoop which calculates the required motor duty cycle based
#on the error between a reference position and velocity and the motor's actual position and
#velocity. The PID controller also calculates its performance over time, denoted as J, using
#the sum of the squares of the position and velocity error. J provides a metric to compare
#different controllers. The controller task controller.Controller uses the PID controller to move
#the motor along a reference motion profile.\n
#
#The end goal of this project was to have the motor follow a reference profile and velocity, provided as
#comma separated values. To achieve this goal, I created a controller task controller.Controller which
#uses an encoderDriver.Encoder object, pid.ClosedLoop object, and a motorDriver.Motor object to
#control the motor and track the reference profile. The controller task  is set up as a state machine to
#allow for rapid execution on the single threaded Nucleo without interrupting other tasks. In normal
#opperation the controller switches between waiting and moving the motor along the reference profile,
#as shown in Figure 1 below.
#
#\image html contSM.jpg
#
#The Nucleo is very limited on memory, so having too many reference points in the reference file could
#cause memory problems. To avoid this, I created a \link csvResampler.py script \endlink to down sample the
#reference csv into more managable 0.1 second increments before loading it onto the Nucleo.\n
#
#While controlling the motor, the controller task also sends motor position data to the serial
#communication task, nucleoSerial.nucleoSerial via shares which is a script used to share data between
#tasks. The results of this controller design are shown on the \ref finalproject "overview page" of this project.
#
