"""@file me405_lab3_front_end.py
@brief This module runs on a pc and sends and recieves data to the nucleo.
@details This module first prompts the user to input an ASCII character to send to the nucleo,
which is used to signal to the nucleo that it should start collecting data. Then, this module
reads data from the nucleo via serial and prints it out.
@author Eliot Briefer
@date May 6, 2020
"""
import serial

ser = serial.Serial(port='COM3',baudrate=115200,timeout=1)
data = []

def sendChar():
    '''
    @brief Method to send an ascii character to the Nucleo over serial.
    @details When called, this method prompts the user to input a character,
    then waits for user input
    @return the value sent over serial
    '''
    inv = input('Give me a character: ')
    ser.write(str(inv).encode())
    myval = ser.readline().decode()
    return myval

def readLine():
    '''
    @brief A method to read from a line from the serial port.
    @return An ASCII string representation of the line read from serial.
    '''
    return ser.readline().decode('ascii')

if __name__ == '__main__':
    
    while True:
        sendChar()
        while len(data) < 5000:
            temp = readLine()
            if temp != '':
                data.append([len(data) / 50000, temp]) #[[time, adc measurment], ...]
        print(data)