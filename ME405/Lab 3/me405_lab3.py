"""@file me405_lab3.py
@brief This module runs on the nucleo and sends and recieves data from the pc.
@details When the user sends the nucleo 'g' from me405_lab3_front_end.py the nucleo
begins reading adc data from the blue user button. Once the nucleo has captured a button release
it sends the recorded adc data to the pc.
@author Eliot Briefer
@date May 6, 2020
"""

import pyb, array
from pyb import UART

myuart = UART(3) #for serial coms
pyb.repl_uart(None) #don't send errors or print statements

adcBuffer = array.array('H', (0 for i in range(5000)))
pinA1 = pyb.Pin.cpu.A1
adcTimer = pyb.Timer(1, freq=50000)
adc1 = pyb.ADC(pinA1)

record = False #If true record adc data to buffer
    
def send(data):
    '''
    @brief A method used to send data to the pc over serial.
    @details This method writes each element in data to serial one line at a time.
    @param data An itterable object to write line by line to serial. (typically a list or array)
    '''
    for i in data:
        #print('Sending: ' + i)
        myuart.write(i + '\r\n')

if __name__ == '__main__':
    while True:
        if myuart.any() != 0:
            val = myuart.readchar()
            if str(val).lower() == 'g':
                record = True #only record data after user presses g
        if record == True:
            adc1.read_timed(adcBuffer, adcTimer)
            if adcBuffer[0] < 600 and adcBuffer[-1] > 3800: #Check if button release has occured
                print('button released')
                record = False
                send(adcBuffer)
                