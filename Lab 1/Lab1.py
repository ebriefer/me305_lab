'''@file Lab1.py
@brief A program that calculates the fibonacci number at given index.
@details This program prompts the user to input an index for the fibonacci
sequence into the console. It then prints out the fibonacci number at that
index to the console. If the user inputs a non-integer or negative value the
program will prompt the user to enter a valid index. Note that problems occur
with indexes larger than 1 million due to the fibonacci numbers becoming
extremely large.
@author Eliot Briefer
'''

def fib (idx):
    
    '''@brief This function calculates the fibonacci sequence
    @details Note this function only works for positive integer indexes
    @param idx An integer specifying the index in the fibonacci sequence to find
    @return The associated Fibonacci number at the specified index
    '''
    
    if idx < 0: #don't allow negative indexes
        raise IndexError()
        
    ##@brief A list used to store the fibbonacci sequence up to the given index
    f = [0, 1]
    
    for _ in range(2, idx + 1): #generate fibonacci sequence up to requested index
        f.append(f[-1] + f[-2])
    #print(f)
    return f[idx] #return value at requested index
  
if __name__ == '__main__':
    while True:
        try:
            requestedInx = input('Enter a Fibonacci index: ')
            print('Fibonacci number at index {:} is {:}.'.format(int(requestedInx),fib(int(requestedInx))))
            break
        
        except:
            print("Thats not a valid index, try again!")
    

    


